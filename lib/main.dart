import 'package:flutter/material.dart';
import 'package:phicos_mentoring/page/form_page.dart';
import 'package:phicos_mentoring/page/form_page.dart';
import 'package:phicos_mentoring/page/ganti_warna_page.dart';
import 'package:phicos_mentoring/page/list_halaman.dart';
import 'package:phicos_mentoring/page/get_json.dart';
import 'package:phicos_mentoring/page/get_json.dart';
import 'package:phicos_mentoring/page/bottom_bar.dart';
import 'package:phicos_mentoring/page/bottom_bar.dart';
import 'package:phicos_mentoring/page/animasi_hero.dart';
import 'package:phicos_mentoring/page/animasi_hero.dart';
import 'package:phicos_mentoring/page/no_scaffold.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: GantiWarnaPage(), // belajar provider
//      home: MyHomePage(title: 'Flutter Demo Home Page'),
      routes: {
        '/tanpa-scaffold': (ctx) {
          return NoScaffold();
        },
        ListHalaman.routeName: (ctx) => ListHalaman(),
        AnimasiHero.routeName: (ctx) => AnimasiHero(),
        BottomBar.routeName: (ctx) => BottomBar(),
        GetDataJson.routeName: (ctx) => GetDataJson(),
        FormPage.routeName: (ctx) => FormPage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(ListHalaman.routeName);
                },
                child: Text('Halaman 2')),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
