import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {
  static const routeName = '/form-page';
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  TextEditingController namaController;
  TextEditingController nohapeController;
  String nama;
  String noHape;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Form Page'),
      ),
      body: Column(
        children: <Widget>[
          TextField(
            onChanged: (val) {
              setState(() {
                nama = val;
              });
            },
            controller: namaController,
            decoration: InputDecoration(
                labelText: 'Nama',
                hintText: 'isikan nama disini',
                prefixIcon: Icon(Icons.person)),
            keyboardType: TextInputType.text,
          ),
          Text(nama ?? 'kosong'),
          RaisedButton(
            onPressed: () {
              setState(() {});
            },
            child: Text('Simpan'),
          )
        ],
      ),
    );
  }
}
