import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:phicos_mentoring/page/animasi_hero.dart';
import 'package:phicos_mentoring/page/form_page.dart';
import 'package:phicos_mentoring/page/get_json.dart';
import 'package:phicos_mentoring/page/bottom_bar.dart';

class ListHalaman extends StatelessWidget {
  static const routeName = '/list-halaman';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Halaman'),
      ),
      body: Column(
        children: [
          Container(
            height: 70,
            margin: EdgeInsets.all(8),
            decoration: BoxDecoration(
                border: Border.all(width: 3),
                borderRadius: BorderRadius.circular(10)),
            child: ListTile(
              leading: Image.asset(
                'assets/image/naruto.png',
                fit: BoxFit.fill,
              ),
              title: FlatButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed('/tanpa-scaffold');
                  },
                  child: Text('Tanpa Scaffold')),
              trailing: Icon(Icons.arrow_forward),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border(
                    top: BorderSide(width: 3),
                    bottom: BorderSide(width: 3),
                    right: BorderSide(width: 3),
                    left: BorderSide(width: 3),
                  )),
              child: InkWell(
                splashColor: Colors.purple,
                onTap: () {
                  Navigator.of(context).pushNamed(AnimasiHero.routeName);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Hero(
                      tag: 'aang',
                      child: Container(
                        height: 70,
                        width: 70,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/image/aang.png'),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Text('Animasi Hero'),
                    Icon(Icons.arrow_forward),
                  ],
                ),
              ),
            ),
          ),
          buildButton(
              label: 'Bottom bar',
              onpress: () {
                Navigator.pushReplacementNamed(context, BottomBar.routeName);
              }),
          buildButton(
              label: 'get Data Json',
              onpress: () {
                Navigator.pushNamed(context, GetDataJson.routeName);
              },
              color: Colors.purple),
//          buildButton(
//            label: 'Form Page',
//            onpress: () {
//              Navigator.pushNamed(context, FormPage.routeName);
//            },
//          ),
        ],
      ),
    );
  }

  Padding buildButton(
      {String label = 'kosong', Function onpress, Color color}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        height: 70,
        width: double.infinity,
        child: RaisedButton(
          color: color ?? Colors.pink,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          onPressed: onpress,
          child: Text(
            '$label',
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
