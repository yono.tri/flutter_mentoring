import 'package:flutter/material.dart';

class GantiWarnaPage extends StatefulWidget {
  @override
  _GantiWarnaPageState createState() => _GantiWarnaPageState();
}

class _GantiWarnaPageState extends State<GantiWarnaPage> {
  bool warna = true;
  @override
  Widget build(BuildContext context) {
    print('build ulang');
    return Scaffold(
      appBar: AppBar(
        title: buildTextAppbar(),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          buildContainerWarna(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              buildTextMerah(),
              Switch(
                onChanged: (val) {
                  setState(() {
                    warna = !warna;
                  });
                },
                value: warna,
              ),
              buildTextIjo(),
            ],
          )
        ],
      ),
    );
  }

  Text buildTextAppbar() {
    print('build ulang text appbar');
    return Text('Ganti Warna');
  }

  Container buildContainerWarna() {
    print(' build ulang container warna');
    return Container(
      height: 200,
      width: 200,
      color: warna ? Colors.green : Colors.red,
    );
  }

  Text buildTextIjo() {
    print('build ulang warna ijo');
    return Text(
      'Warna Ijo',
      style: TextStyle(color: Colors.black),
    );
  }

  Text buildTextMerah() {
    print('build ulang warna merah');
    return Text(
      'Warna Merah',
      style: TextStyle(color: Colors.black),
    );
  }
}
