import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:phicos_mentoring/page/list_halaman.dart';

class BottomBar extends StatefulWidget {
  static const routeName = '/halaman-5';
  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bottom Bar'),
        automaticallyImplyLeading: false,
      ),
      body: ListView.builder(
        itemCount: 100,
        itemBuilder: (ctx, index) => ListTile(
          leading: Icon(Icons.person),
          title: Text('nama $index'),
          trailing: Icon(Icons.arrow_forward),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            IconButton(
                icon: Icon(Icons.home),
                onPressed: () {
                  Navigator.of(context)
                      .pushReplacementNamed(ListHalaman.routeName);
                }),
            Text('Halaman 2')
          ],
        ),
      ),
    );
  }
}
