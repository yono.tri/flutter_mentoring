import 'package:flutter/material.dart';

class AnimasiHero extends StatefulWidget {
  static const routeName = '/animasi-hero';
  @override
  _AnimasiHeroState createState() => _AnimasiHeroState();
}

class _AnimasiHeroState extends State<AnimasiHero> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('animasi hero'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Hero(
            tag: 'aang',
            child: Image.asset('assets/image/aang.png'),
          ),
          Text(
            'Avatar Aang',
            style: TextStyle(fontSize: 30),
          )
        ],
      ),
    );
  }
}
