import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:phicos_mentoring/model/contact_model.dart';

class GetDataJson extends StatefulWidget {
  static const routeName = '/halaman-6';
  @override
  _GetDataJsonState createState() => _GetDataJsonState();
}

class _GetDataJsonState extends State<GetDataJson> {
  Contact contact;
  List<Data> listData;
  String url = 'https://reqres.in/api/users?page=2';

  void getData() async {
    final response = await http.get(url);
    contact = Contact.fromJson(jsonDecode(response.body));
    setState(() {
      listData = contact.data;
      print(listData[0].firstName);
    });
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List contact from json'),
      ),
      body: listData == null
          ? Center(child: CircularProgressIndicator())
          : ListView.builder(
              itemCount: listData.length,
              itemBuilder: (ctx, index) => ItemContact(
                    no: '${index + 1}',
                    nama: listData[index].firstName,
                    email: listData[index].email,
                    urlImage: listData[index].avatar,
                  )),
    );
  }
}

class ItemContact extends StatelessWidget {
  const ItemContact({
    Key key,
    this.no,
    this.nama,
    this.email,
    this.urlImage,
  }) : super(key: key);

  final String no;
  final String nama;
  final String email;
  final String urlImage;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Text('$no'),
      title: Text('$nama'),
      subtitle: Text('$email'),
      trailing: CircleAvatar(
        backgroundImage: NetworkImage('$urlImage'),
      ),
    );
  }
}
